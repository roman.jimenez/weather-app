require 'spec_helper'
require 'ostruct'
require 'webmock/rspec'
require_relative '../../app/services/address_service'

describe AddressService do
  describe '.get_address_information' do
    context 'when given a valid address' do
      before do
        @address = '1600 Amphitheatre Parkway, Mountain View, CA 94043'
        @lat = '37.4224082'
        @lon = '-122.0856086'
        @display_name = 'Google Building 41, 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA'
        @api_response = {
          'features' => [{            
            'properties' => {              
                'lat' => @lat, 
                'lon' => @lon,
                'formatted' => @display_name            
               }          
            }],
          'query' => {
            'text' => @address
          }
        }.to_json
        stub_request(:get, "#{AddressService::LOCATION_API_ENDPOINT}?apiKey=#{AddressService::LOCATION_API_KEY}&text=#{@address}")
          .to_return(status: 200, body: @api_response)
      end

      it 'returns a OpenStruct with the lat, lon and display_name' do
        address_model = AddressService.get_address_information(@address)
        expect(address_model.lat).to eq(@lat)
        expect(address_model.lon).to eq(@lon)
        expect(address_model.display_name).to eq(@display_name)
        expect(address_model.error).to be_nil
      end
    end

    context 'when given an invalid address' do
      before do
        @address = 'Invalid address'
        @api_response = {
          'type' => 'FeatureCollection',
          'query' => {
            'text' => @address
          },
          'features' => []
        }.to_json
        stub_request(:get, "#{AddressService::LOCATION_API_ENDPOINT}?apiKey=#{AddressService::LOCATION_API_KEY}&text=#{@address}")
          .to_return(status: 200, body: @api_response)
      end

      it 'returns a OpenStruct with the error message' do
        address_model = AddressService.get_address_information(@address)
        expect(address_model.error).to eq("No address found for #{@address}")
        expect(address_model.lat).to be_nil
        expect(address_model.lon).to be_nil
        expect(address_model.display_name).to be_nil
      end
    end

    context 'when there is an error making the API call' do
      before do
        @address = '1600 Amphitheatre Parkway, Mountain View, CA 94043'
        stub_request(:get, "#{AddressService::LOCATION_API_ENDPOINT}?apiKey=#{AddressService::LOCATION_API_KEY}&text=#{@address}")
          .to_raise(StandardError.new('Unable to connect to the server'))
      end

      it 'returns a OpenStruct with the error message' do
        address_model = AddressService.get_address_information(@address)
        expect(address_model.error).to eq('Unable to connect to the server')
        expect(address_model.lat).to be_nil
        expect(address_model.lon).to be_nil
        expect(address_model.display_name).to be_nil
      end
    end
  end
end
