class WeatherController < ApplicationController
  def index
    @data = WeatherService.get_forecast_by_address(params[:address])
  end
end
