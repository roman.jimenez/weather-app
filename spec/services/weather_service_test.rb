require 'spec_helper'
require 'ostruct'
require 'webmock/rspec'
require_relative '../../app/services/weather_service'
require_relative '../../app/services/address_service'
require_relative '../../app/services/cache_weather_service'

RSpec.describe WeatherService do
  describe '.get_forecast_by_address' do
    context 'with a valid address' do
      let(:valid_address) { '1600 Amphitheatre Parkway, Mountain View, CA' }
      let(:address_info) { OpenStruct.new(lat: '37.422', lon: '-122.084', display_name: 'Googleplex, Mountain View, CA') }
      let(:weather_info) { OpenStruct.new(weather_status: 'Sunny', degrees: OpenStruct.new(temp: 78, feels_like: 80)) }

      before do
        allow(AddressService).to receive(:get_address_information).with(valid_address).and_return(address_info)
        allow(WeatherService).to receive(:get_weather_info_from_address).with(address_info).and_return(weather_info)
        allow(CacheWeatherService).to receive(:read).with(anything)
        allow(CacheWeatherService).to receive(:write).with(anything, anything)
      end

      it 'returns the weather forecast for the address' do
        forecast = WeatherService.get_forecast_by_address(valid_address)
        expect(forecast.display_name).to eq('Googleplex, Mountain View, CA')
        expect(forecast.weather.weather_status).to eq('Sunny')
        expect(forecast.weather.degrees.temp).to eq(78)
      end
    end

    context 'with an invalid address' do
      let(:invalid_address) { 'This is not a valid address' }
      let(:address_info) { OpenStruct.new(error: 'Invalid address') }

      before do
        allow(AddressService).to receive(:get_address_information).with(invalid_address).and_return(address_info)
      end

      it 'returns the address error message' do
        forecast = WeatherService.get_forecast_by_address(invalid_address)
        expect(forecast.error).to eq('Invalid address')
      end
    end
  end
end
