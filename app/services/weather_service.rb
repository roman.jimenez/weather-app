require 'uri'
require 'net/http'

class WeatherService
    # WEATHER_API_ENDPOINT = ENV['WEATHER_API_ENDPOINT']
    WEATHER_API_ENDPOINT = "http://api.openweathermap.org/data/2.5/weather"
    # The id is provided by the openweathermap API
    WEATHER_API_APP_ID = "524901"
    # WEATHER_API_KEY = ENV['WEATHER_API_KEY']
    WEATHER_API_KEY = "cda77a8de42f31ad48361da9aa0d2ef1"

    def self.get_forecast_by_address(address)
        address_information = get_address_information(address)
        return address_information unless address_information.error.nil?
        forecast = OpenStruct.new
        cached = CacheWeatherService.read(address_information.key)
        puts cached
        return cached unless cached.nil?
        puts 'no cache'
        forecast.cached = false
        forecast.display_name = address_information.display_name
        forecast.weather = get_weather_info_from_address(address_information)
        unless forecast.success
            forecast.error = forecast.error
            forecast
        end
        CacheWeatherService.write(address_information.key, forecast)
        forecast
    end

    private 

    def self.get_weather_info_from_address(address_information)
        weather = OpenStruct.new()
        begin
            uri = URI("#{WEATHER_API_ENDPOINT}")
            params = {
                :id => WEATHER_API_APP_ID, 
                :appId => WEATHER_API_KEY,
                :lat => address_information.lat,
                :lon => address_information.lon,
                # Should be able to pass this in from front end
                :units => "imperial"
            }
            uri.query = URI.encode_www_form(params)
            response = Net::HTTP.get_response(uri)
            parsed_response = JSON.parse(response.body)
            weather.weather_status = parsed_response['weather'].first['main']
            weather.degrees = OpenStruct.new(parsed_response['main'])
            weather
        rescue => exception
            weather.error = exception.message
            weather
        end
    end

    def self.get_address_information(address)
        AddressService.get_address_information(address)
    end
end