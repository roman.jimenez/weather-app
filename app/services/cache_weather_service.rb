class CacheWeatherService
  def self.read(key)
    Rails.cache.read(key)
  end

  def self.write(key, value, ttl = 1800)
    value.cached = true
    Rails.cache.write(key, value, expires_in: ttl)
  end
end
