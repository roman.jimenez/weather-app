class AddressService
    LOCATION_API_ENDPOINT = "https://api.geoapify.com/v1/geocode/search"
    LOCATION_API_KEY = "ee586dfe9deb4c2c963df20eb73bb75a"
    def self.get_address_information(address)
        self.call(address)
    end

    private 

    def self.call(address)
        address_model = OpenStruct.new()
        begin
            res = api_call(address)
            if res.is_a?(Net::HTTPSuccess)
                address_model = parse_address_response(res.body)
                address_model
            else
               address_model.error = JSON.parse(res.body)['message']
               address_model
            end
        rescue => exception
            address_model.error = exception.message
            address_model
        end
    end

    def self.api_call(address)
        uri = URI("#{LOCATION_API_ENDPOINT}")
        params = {:text => address, :apiKey => LOCATION_API_KEY}
        uri.query = URI.encode_www_form(params)
        Net::HTTP.get_response(uri)
    end

    def self.parse_address_response(body)
        result = JSON.parse body
        address_model = OpenStruct.new()
        if result['features'].length > 0
             properties = OpenStruct.new(result['features'][0]['properties'])
             address_model.lat = properties.lat
             address_model.lon = properties.lon
             address_model.display_name = properties.formatted
             address_model.key = get_city_or_postcode(properties)
             address_model
        else
             address_model.error = "No address found for #{result['query']['text']}"
             address_model
        end
    end

    def self.get_city_or_postcode(properties)
        puts properties
        return properties.postcode unless properties.postcode.nil?
        properties.city
    end
end