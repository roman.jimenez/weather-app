# README

You can run this project using

`bin/rails server`

and it can be accessed via

`localhost:3000/`

to run the tests simply run

`rspec spec/services/*`
