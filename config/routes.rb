Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  get 'weather/index'
  # Default the weather path as root ("/")
  root 'weather#index'
end
